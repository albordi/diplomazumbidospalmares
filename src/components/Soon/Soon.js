import MainImage from '../MainImage/MainImage';
import './Soon.css';

const Soon = () => {
  return (
    <div className='soon'>
      <div className='container'>
        <div className='banner'>
          <MainImage className='mt-10' />
        </div>
        <p className='title text-center mt-30'>Esse site será lançado no dia 20 de Novembro de 2023 na Câmara Municipal de Campinas</p>
        <img src={require('../../assets/images/inauguracao.png')} alt='Imagem de fitas para ser inaugurada' />
        <div className='main-text'>
          <p className='text-center text'>
            A Câmara Municipal de Campinas outorga anualmente o diploma Zumbi
            dos Palmares às pessoas que mais se destacaram na defesa, na
            integração social dos membros da comunidade negra de Campinas, bem
            como na difusão da cultura afro-brasileira. Esse projeto tem como
            objetivo fazer uma pesquisa e um mapeamento das pessoas que foram
            homenageadas com o diploma, bem como publicizar as informações em um
            site de acesso público, onde possamos acessar esses dados e as
            biografias dessas pessoas.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Soon;
