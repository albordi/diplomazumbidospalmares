import './Photo.css';

const Photo = ({ photo }) => {
  return <img className='Photo' src={photo} alt='Foto do homenageado' />;
};

export default Photo;
