export const filterData = (honored, searchText) => {
  const filteredData = honored.filter((honor) => {
    return (
      honor.honorYear === searchText ||
      honor.name.toLowerCase().includes(searchText.toLowerCase())
    );
  });
  return filteredData;
};